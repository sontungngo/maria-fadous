import time
from datetime import datetime
import numpy as np
from os.path import exists
import pandas as pd
import random


# class restaurant client to simulate a restaurant like experience
class Gen_Client:
    type = ""
    idList = []
    id = ""
    first_course = ["Soup", "Tomato-Mozarella", "Oysters"]
    second_course = ["Salad", "Spaghetti", "Steak", "Lobster"]
    third_course = ["Ice cream", "Pie"]
    course1 = ""
    course2 = ""
    course3 = ""

    # constructor that checks and creates a new client
    def __init__(self, c_id,id_df):
        new_client = True
        print(id_df)
        for client in id_df:
            if c_id == client:
                new_client = False
        if new_client:
            c_type=input("Enter Client Type.")
            self.idList.append(c_id)
            self.id = c_id
            self.type = c_type

    # function to simulate the restaurant simulation
    def goToRestaurant(self):
        order1 = 0
        order2 = 0
        order3 = 0
        print("Welcome\n"
              "Menu: \n\t"
              "First Course:\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1] + ":2\n\t\t*" +
              self.first_course[2] +
              ":3\n\t"
              "Second Course:\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] + ":2\n\t\t*" +
              self.second_course[2] +
              ":3\n\t\t*" + self.second_course[3] + ":4\n\t"
                                                    "Third Course:\n\t\t*" + self.third_course[0] + ":1\n\t\t*" +
              self.third_course[1] + ":2")

        # starter course input
        print("Choose your starter?Enter 0 for nothing choose 1-3 for food")
        # check input validity and add starter order
        is_int = False
        while not is_int:
            try:
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1]
                      + ":2\n\t\t*" + self.first_course[2] + ":3")
                order1 = int(input("enter number here:"))
                is_int = True
                if order1 == 0:
                    self.course1 = "none"
                    print("You have chosen " + self.course1)
                elif order1 == 1:
                    self.course1 = self.first_course[0]
                    print("You have chosen " + self.course1)
                elif order1 == 2:
                    self.course1 = self.first_course[1]
                    print("You have chosen " + self.course1)
                elif order1 == 3:
                    self.course1 = self.first_course[2]
                    print("You have chosen " + self.course1)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1]
                      + ":2\n\t\t*" + self.first_course[2] + ":3")
                is_int = False

        # main course input
        print("Choose your Main dish?Enter 0 for nothing choose 1-4 for food.")
        # check input validity and add main order
        is_int = False
        while not is_int:
            try:
                order2 = int(input("enter number here:"))
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] +
                      ":2\n\t\t*" + self.second_course[2] + ":3\n\t\t*" + self.second_course[3] + ":4")
                is_int = True
                if order2 == 0:
                    self.course2 = "none"
                    print("You have chosen " + self.course2)
                elif order2 == 1:
                    self.course2 = self.second_course[0]
                    print("You have chosen " + self.course2)
                elif order2 == 2:
                    self.course2 = self.second_course[1]
                    print("You have chosen " + self.course2)
                elif order2 == 3:
                    self.course2 = self.second_course[2]
                    print("You have chosen " + self.course2)
                elif order2 == 4:
                    self.course2 = self.second_course[3]
                    print("You have chosen " + self.course2)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] +
                      ":2\n\t\t*" + self.second_course[2] + ":3\n\t\t*" + self.second_course[3] + ":4")
                is_int = False

        # dessert course input
        print("Choose your Dessert?Enter 0 for nothing choose 1-3 for food.")
        # check input validity and add dessert order
        is_int = False
        while not is_int:
            try:
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.third_course[0] + ":1\n\t\t*" + self.third_course[1] +
                      ":2")
                order3 = int(input("enter number here:"))
                is_int = True
                if order3 == 0:
                    self.course3 = "none"
                    print("You have chosen " + self.course3)
                elif order3 == 1:
                    self.course3 = self.third_course[0]
                    print("You have chosen " + self.course3)
                elif order3 == 2:
                    self.course3 = self.third_course[1]
                    print("You have chosen " + self.course3)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                is_int = False

        # get current time
        timestamp = time.time()
        # convert to datetime
        date_time = datetime.fromtimestamp(timestamp)
        # convert timestamp to string in dd-mm-yyyy HH:MM:SS
        str_date_time = date_time.strftime("%d-%m-%Y, %H:%M:%S")

        # save the order and get it as an object
        df_final = save_Courses(self.course1, self.course2, self.course3, str_date_time, self.id)

        # create or append the orders to a csv file
        if exists("../Data/part4.csv"):
            df_old = pd.read_csv("../Data/part4.csv", index_col=0)
            df_old.loc[len(df_old.index)] = [self.id, self.course1, self.course2, self.course3, str_date_time]
            df_old.to_csv("../Data/part4.csv", index=True)
        else:
            df_final.df_new.to_csv("../Data/part4.csv")


# class restaurant courses to save and update client data from the restaurant simulation
class save_Courses:
    first_order = []
    second_order = []
    third_order = []
    c_id = []
    order_time = []
    df_new = pd.DataFrame()

    def __init__(self, first_ord,second_ord,third_ord,time_ord,cli_id):
        self.first_order.append(first_ord)
        self.second_order.append(second_ord)
        self.third_order.append(third_ord)
        self.order_time.append(time_ord)
        self.c_id.append(cli_id)
        self.updateDataFrame()

    def updateDataFrame(self):
        df_id = np.array(self.c_id, dtype='object')
        self.df_new["id"] = df_id.tolist()

        df_course1 = np.array(self.first_order, dtype='object')
        self.df_new["first course"] = df_course1.tolist()

        df_course2 = np.array(self.second_order, dtype='object')
        self.df_new["second course"] = df_course2.tolist()

        df_course3 = np.array(self.third_order, dtype='object')
        self.df_new["third course"] = df_course3.tolist()

        df_time = np.array(self.order_time, dtype='object')
        self.df_new["time"] = df_time.tolist()

def main():
    # make a random id for each customer
    client_id = 'cl'+ str(random.randrange(100000, 999999))
    #check if already id exist
    if exists("../Data/part4.csv"):
        df=pd.read_csv("../Data/part4.csv")
        client = Gen_Client(client_id,df['id'])
        client.goToRestaurant()
    else:
        idd=[]
        client = Gen_Client(client_id,idd)
        client.goToRestaurant()

if __name__ == '__main__':
    main()
# main function to initiate the restaurant simulation


first_course = ["Soup", "Tomato-Mozarella", "Oysters"]
second_course = ["Salad", "Spaghetti", "Steak", "Lobster"]
third_course = ["Ice cream", "Pie"]
Client_type=['Business','OneTime','Healthy','Retirement']
id=[]
ctype=[]
course1=[]
course2=[]
course3=[]
timecl=[]
drink_1=[]
drink_2=[]
drink_3=[]
total_1=[]
total_2=[]
total_3=[]


for i in range(1,36502):
  timestamp = time.time()
      # convert to datetime
  date_time = datetime.fromtimestamp(timestamp)
      # convert timestamp to string in dd-mm-yyyy HH:MM:SS
  str_date_time = date_time.strftime("%d-%m-%Y, %H:%M:%S")
  timecl.append(str_date_time)
  CUSTOMERID = 'cl'+ str(random.randrange(100000, 999999))
  id.append(CUSTOMERID)
 #for 1st course,drink and total
  firstC_num=int(random.randrange(0, 2))
  firstC=first_course[firstC_num]
  course1.append(firstC)
  if (firstC_num==0):
    drink1=float(random.randrange(0.0, 5.0))
    total1=drink1+3
    drink_1.append(drink1)
    total_1.append(total1)
  elif(firstC_num==1):
    drink1=float(random.randrange(0.0, 4.0))
    total1=drink1+15
    drink_1.append(drink1)
    total_1.append(total1)
  else:
    drink1=float(random.randrange(0.0, 7.0))
    total1=drink1+20.0
    drink_1.append(drink1)
    total_1.append(total1)
#for 2nd course, drink and total
  secondC_num=int(random.randrange(0, 3))
  secondC=second_course[secondC_num]
  course2.append(secondC)
  if (secondC_num==0):
        drink2=float(random.randrange(0.0, 5.0))
        total2=drink2+9.0
        drink_2.append(drink2)
        total_2.append(total2)
  elif(secondC_num==1):
        drink2=float(random.randrange(0.0, 4.0))
        total2=drink2+20.0
        drink_2.append(drink2)
        total_2.append(total2)
  elif(secondC_num==2):
        drink2=float(random.randrange(0.0, 4.0))
        total2=drink2+25.0
        drink_2.append(drink2)
        total_2.append(total2)
  else:
        drink2=float(random.randrange(0.0, 7.0))
        total2=drink2+40.0
        drink_2.append(drink2)
        total_2.append(total2)
#for 3rd course,drink and total
  thirdC_num=int(random.randrange(0, 1))
  thirdC=third_course[thirdC_num]
  course3.append(thirdC)
  if (secondC_num==0):
        drink3=float(random.randrange(0.0, 5.0))
        total3=drink3+15.0
        drink_3.append(drink3)
        total_3.append(total3)
  else:
        drink3=float(random.randrange(0.0, 7.0))
        total3=drink3+10.0
        drink_3.append(drink3)
        total_3.append(total3)
#for client type
  cl_type=Client_type[int(random.randrange(0, 3))]
  ctype.append(cl_type)


d = {'TIME':timecl,'CUSTOMERID':id,'CUSTOMERTYPE':ctype,'COURSE1':course1,'COURSE2':course2,'COURSE3':course3,'DRINKS1':drink_1,'DRINKS2':drink_2,
     'DRINKS3':drink_3,'TOTAL1':total_1,'TOTAL2':total_2,'TOTAL3':total_3}




if exists("../Data/part5.csv"):
    df_old = pd.read_csv("../Data/part5.csv", index_col=0)
    print('file exists')
else:
  data=pd.DataFrame(d)
  data.to_csv("../Data/part5.csv")
