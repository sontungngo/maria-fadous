import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

input1 = "../Data/part1.csv"

Mydataframe = pd.read_csv(os.path.abspath(input1), sep=",")

input2 = "../Data/part3.csv"
Mydataframe2 = pd.read_csv(os.path.abspath(input2), sep=",")

# code to compare labels#
label = Mydataframe['LABEL']
client_type = Mydataframe2['CLIENT_TYPE']
labeltype = []
for i in label:
    if (i == 0):
        labeltype.append('Business')
    elif (i == 1):
        labeltype.append('Retirement')
    elif (i == 2):
        labeltype.append('Healthy')
    else:
        labeltype.append('Onetime')
print(labeltype)
n = 0
true_predicted = 0
false_predicted = 0
for i in Mydataframe2['CLIENT_TYPE']:
    if (i == labeltype[n]):
        true_predicted = true_predicted + 1
    else:
        false_predicted = false_predicted + 1
    n += 1
print(true_predicted, false_predicted)

# code to plot distribution of clients

dfcluster0 = Mydataframe[Mydataframe['LABEL'] == 0]
dfcluster1 = Mydataframe[Mydataframe['LABEL'] == 1]
dfcluster2 = Mydataframe[Mydataframe['LABEL'] == 2]
dfcluster3 = Mydataframe[Mydataframe['LABEL'] == 3]

cluster1 = dfcluster0['LABEL']
cluster2 = dfcluster1['LABEL']
cluster3 = dfcluster2['LABEL']
cluster4 = dfcluster3['LABEL']

plt.bar('Business', len(cluster1), color='green', label='Business')
plt.bar('Retirement', len(cluster2), color='blue', label='Retirement')
plt.bar('Healthy', len(cluster3), color='red', label='Healthy')
plt.bar('Onetime', len(cluster4), color='black', label='Onetime')
plt.gca().set(title='distribution of clients', ylabel='number of clients', xlabel='client type')
plt.show()


# probablity for business client to order each course
firstcourse = (len(cluster1) - len(dfcluster0[dfcluster0['FIRST_COURSE'] == 0.0]))
print('Probablity to order first course for business client', (firstcourse / len(cluster1)) * 100, '%')
secondcourse = (len(cluster1) - len(dfcluster0[dfcluster0['SECOND_COURSE'] == 0.0]))
print('Probablity to order second course for business client', (secondcourse / len(cluster1)) * 100, '%')
thirdcourse = (len(cluster1) - len(dfcluster0[dfcluster0['THIRD_COURSE'] == 0.0]))
print('Probablity to order second course for business client', (thirdcourse / len(cluster1)) * 100, '%')

# probablity for retirement client to order each course
firstcourse = (len(cluster2) - len(dfcluster1[dfcluster1['FIRST_COURSE'] == 0.0]))
print('Probablity to order first course for Retirement client', (firstcourse / len(cluster2)) * 100, '%')
secondcourse = (len(cluster2) - len(dfcluster1[dfcluster1['SECOND_COURSE'] == 0.0]))
print('Probablity to order second course for Retirement client', (secondcourse / len(cluster2)) * 100, '%')
thirdcourse = (len(cluster2) - len(dfcluster1[dfcluster1['THIRD_COURSE'] == 0.0]))
print('Probablity to order second course for Retirement client', (thirdcourse / len(cluster2)) * 100, '%')

# probablity for healthy client to order each course
firstcourse = (len(cluster3) - len(dfcluster2[dfcluster2['FIRST_COURSE'] == 0.0]))
print('Probablity to order first course for healthy client', (firstcourse / len(cluster3)) * 100, '%')
secondcourse = (len(cluster3) - len(dfcluster2[dfcluster2['SECOND_COURSE'] == 0.0]))
print('Probablity to order second course for healthy client', (secondcourse / len(cluster3)) * 100, '%')
thirdcourse = (len(cluster3) - len(dfcluster2[dfcluster2['THIRD_COURSE'] == 0.0]))
print('Probablity to order second course for healthy client', (thirdcourse / len(cluster3)) * 100, '%')

# probablity for onetime client to order each course
firstcourse3 = (len(cluster4) - len(dfcluster3[dfcluster3['FIRST_COURSE'] == 0.0]))
print('Probablity to order first course for onetime client', (firstcourse3 / len(cluster4)) * 100, '%')
secondcourse3 = (len(cluster4) - len(dfcluster3[dfcluster3['SECOND_COURSE'] == 0.0]))
print('Probablity to order second course for onetime client', (secondcourse3 / len(cluster4)) * 100, '%')
thirdcourse3 = (len(cluster4) - len(dfcluster3[dfcluster3['THIRD_COURSE'] == 0.0]))
print('Probablity to order third course for onetime client', (thirdcourse3 / len(cluster4)) * 100, '%')



#probablity of dish for first course
soup=len(dfcluster0[dfcluster0['FIRST_FOOD'] == 3.0])
print('business client ordering soup',(soup/len(dfcluster0['FIRST_FOOD']))*100)
tomato_mozarella=len(dfcluster0[dfcluster0['FIRST_FOOD'] == 15.0])
print('business client ordering tomato_mozarella',(tomato_mozarella/len(dfcluster0['FIRST_FOOD']))*100)
oyester=len(dfcluster0[dfcluster0['FIRST_FOOD'] == 20.0])
print('business client ordering oyester',(oyester/len(dfcluster0['FIRST_FOOD']))*100)

#probablity of dish for main course
salad=len(dfcluster0[dfcluster0['SECOND_FOOD'] == 9.0])
print('business client ordering salad',(salad/len(dfcluster0['SECOND_FOOD']))*100)
sphageti=len(dfcluster0[dfcluster0['SECOND_FOOD'] == 20.0])
print('business client ordering sphageti',(sphageti/len(dfcluster0['SECOND_FOOD']))*100)
steak=len(dfcluster0[dfcluster0['SECOND_FOOD'] == 25.0])
print('business client ordering steak',(steak/len(dfcluster0['SECOND_FOOD']))*100)
lobster=len(dfcluster0[dfcluster0['SECOND_FOOD'] == 40.0])
print('business client ordering lobster',(lobster/len(dfcluster0['SECOND_FOOD']))*100)

#probablity of dish for third course
icecream=len(dfcluster0[dfcluster0['THIRD_FOOD'] == 15.0])
print('business client ordering icecream',(icecream/len(dfcluster0['THIRD_FOOD']))*100)
pie=len(dfcluster0[dfcluster0['THIRD_FOOD'] == 10.0])
print('business client ordering pie',(pie/len(dfcluster0['THIRD_FOOD']))*100)






#probablity of dish for first course
soup=len(dfcluster1[dfcluster1['FIRST_FOOD'] == 3.0])
print('Retirement client ordering soup',(soup/len(dfcluster1['FIRST_FOOD']))*100)
tomato_mozarella=len(dfcluster1[dfcluster1['FIRST_FOOD'] == 15.0])
print('Retirement client ordering tomato_mozarella',(tomato_mozarella/len(dfcluster1['FIRST_FOOD']))*100)
oyester=len(dfcluster1[dfcluster1['FIRST_FOOD'] == 20.0])
print('Retirement client ordering oyester',(oyester/len(dfcluster1['FIRST_FOOD']))*100)

#probablity of dish for main course
salad=len(dfcluster1[dfcluster1['SECOND_FOOD'] == 9.0])
print('Retirement client ordering salad',(salad/len(dfcluster1['SECOND_FOOD']))*100)
sphageti=len(dfcluster1[dfcluster1['SECOND_FOOD'] == 20.0])
print('Retirement client ordering sphageti',(sphageti/len(dfcluster1['SECOND_FOOD']))*100)
steak=len(dfcluster1[dfcluster1['SECOND_FOOD'] == 25.0])
print('Retirement client ordering steak',(steak/len(dfcluster1['SECOND_FOOD']))*100)
lobster=len(dfcluster1[dfcluster1['SECOND_FOOD'] == 40.0])
print('Retirement client ordering lobster',(lobster/len(dfcluster1['SECOND_FOOD']))*100)

#probablity of dish for third course
icecream=len(dfcluster1[dfcluster1['THIRD_FOOD'] == 15.0])
print('Retirement client ordering icecream',(icecream/len(dfcluster1['THIRD_FOOD']))*100)
pie=len(dfcluster1[dfcluster1['THIRD_FOOD'] == 10.0])
print('Retirement client ordering pie',(pie/len(dfcluster1['THIRD_FOOD']))*100)




#probablity of dish for first course
soup=len(dfcluster2[dfcluster2['FIRST_FOOD'] == 3.0])
print('Healthy client ordering soup',(soup/len(dfcluster2['FIRST_FOOD']))*100)
tomato_mozarella=len(dfcluster2[dfcluster2['FIRST_FOOD'] == 15.0])
print('Healthy client ordering tomato_mozarella',(tomato_mozarella/len(dfcluster2['FIRST_FOOD']))*100)
oyester=len(dfcluster2[dfcluster2['FIRST_FOOD'] == 20.0])
print('Healthy client ordering oyester',(oyester/len(dfcluster2['FIRST_FOOD']))*100)

#probablity of dish for main course
salad=len(dfcluster2[dfcluster2['SECOND_FOOD'] == 9.0])
print('Healthy client ordering salad',(salad/len(dfcluster2['SECOND_FOOD']))*100)
sphageti=len(dfcluster2[dfcluster2['SECOND_FOOD'] == 20.0])
print('Healthy client ordering sphageti',(sphageti/len(dfcluster2['SECOND_FOOD']))*100)
steak=len(dfcluster2[dfcluster2['SECOND_FOOD'] == 25.0])
print('Healthy client ordering steak',(steak/len(dfcluster2['SECOND_FOOD']))*100)
lobster=len(dfcluster2[dfcluster2['SECOND_FOOD'] == 40.0])
print('Healthy client ordering lobster',(lobster/len(dfcluster2['SECOND_FOOD']))*100)

#probablity of dish for third course
icecream=len(dfcluster2[dfcluster2['THIRD_FOOD'] == 15.0])
print('Healthy client ordering icecream',(icecream/len(dfcluster2['THIRD_FOOD']))*100)
pie=len(dfcluster2[dfcluster2['THIRD_FOOD'] == 10.0])
print('Healthy client ordering pie',(pie/len(dfcluster2['THIRD_FOOD']))*100)




#probablity of dish for first course
soup=len(dfcluster3[dfcluster3['FIRST_FOOD'] == 3.0])
print('Onetime client ordering soup',(soup/len(dfcluster3['FIRST_FOOD']))*100)
tomato_mozarella=len(dfcluster3[dfcluster3['FIRST_FOOD'] == 15.0])
print('Onetime client ordering tomato_mozarella',(tomato_mozarella/len(dfcluster3['FIRST_FOOD']))*100)
oyester=len(dfcluster3[dfcluster3['FIRST_FOOD'] == 20.0])
print('Onetime client ordering oyester',(oyester/len(dfcluster3['FIRST_FOOD']))*100)

#probablity of dish for main course
salad=len(dfcluster3[dfcluster3['SECOND_FOOD'] == 9.0])
print('Onetime client ordering salad',(salad/len(dfcluster3['SECOND_FOOD']))*100)
sphageti=len(dfcluster3[dfcluster3['SECOND_FOOD'] == 20.0])
print('Onetime client ordering sphageti',(sphageti/len(dfcluster3['SECOND_FOOD']))*100)
steak=len(dfcluster3[dfcluster3['SECOND_FOOD'] == 25.0])
print('Onetime client ordering steak',(steak/len(dfcluster3['SECOND_FOOD']))*100)
lobster=len(dfcluster3[dfcluster3['SECOND_FOOD'] == 40.0])
print('Onetime client ordering lobster',(lobster/len(dfcluster3['SECOND_FOOD']))*100)

#probablity of dish for third course
icecream=len(dfcluster3[dfcluster3['THIRD_FOOD'] == 15.0])
print('Onetime client ordering icecream',(icecream/len(dfcluster3['THIRD_FOOD']))*100)
pie=len(dfcluster3[dfcluster3['THIRD_FOOD'] == 10.0])
print('Onetime client ordering pie',(pie/len(dfcluster3['THIRD_FOOD']))*100)


# distribution of drink cost per course
firstC = Mydataframe['FIRST_DRINK']
secondC = Mydataframe['SECOND_DRINK']
thirdC = Mydataframe['THIRD_DRINK']
plt.bar('FIRST_DRINK', firstC, color='g', label='FIRST_DRINK')
plt.bar('SECOND_DRINK', secondC, color='b', label='SECOND_DRINK')
plt.bar('THIRD_DRINK', thirdC, color='r', label='THIRD_DRINK')
plt.gca().set(title='Drink cost per course', ylabel='Drink cost', xlabel='Course')
plt.show()
